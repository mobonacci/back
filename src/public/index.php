<?php

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use Wminded\Fibo;

require '../vendor/autoload.php';
header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");


$app = new \Slim\App;

/**
 * Return the liste of element till nbrMax
 */
$app->get('/getAll/{nbrMax:[0-9]+}', function (Request $request, Response $response, array $args) {
    $nbrMax = $args['nbrMax'];

    $tab = Fibo::createFibo($nbrMax, true)->getTab();

    $response = $response->withJson($tab);
    return $response;
});


/**
 * Return the liste of element +1
 * indice is the actual number of element in the list
 */
$app->get('/goToNext/[{indice:[0-9]+}]', function (Request $request, Response $response, array $args) {
    $indice = $args['indice'];
//    $fibo = new Fibo($indice, false);
//    $fibo->goToNext();
//    $tab = $fibo->getTab();
    $tab = Fibo::createFibo($indice, false)->goToNext()->getTab();

    $response = $response->withJson($tab);
    return $response;
});


/**
 * Return the liste of element -1
 * indice is the actual number of element in the list
 */
$app->get('/goToPrev/[{indice:[0-9]+}]', function (Request $request, Response $response, array $args) {
    $indice = $args['indice'];
//    $fibo = new Fibo($indice, false);
//    $fibo->goToPrev();
//    $tab = $fibo->getTab();
    $tab = Fibo::createFibo($indice, false)->goToPrev()->getTab();

    $response = $response->withJson($tab);
    return $response;
});


$app->get('/[{route:.+}]', function (Request $request, Response $response, array $args) {
    $response = $response->withJson([]);
    return $response;
});

$app->run();
