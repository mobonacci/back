<?php

namespace Wminded;

/**
 * Fibonacci numbers in PHP
 */
class Fibo
{
    # PHP_FLOAT_MAX;

    /**
     * @var array fibonacci serie
     */
    private $tab = [];

    /**
     * @var int The number given as maximum to the serie
     */
    private $maxNumber = 0;

    /**
     * @var int The position of the iteration in the serie we are at
     */
    private $index = 0;


    /**
     * Fibo constructor.
     *
     * If $isMmaxNumber is false it mean the first var is the $index else its the $maxNumber
     *
     * @param int $indice
     * @param bool $isMmaxNumber
     */
    public function __construct(int $indice = 0, $isMmaxNumber = true)
    {
        if ($isMmaxNumber) {
            if ($indice >= 0) {
                $this->maxNumber = $indice;
            } else {
                $this->maxNumber = 0;
            }
            $this->processList();
        } else {
            $this->index = $indice;
            $this->processListByIndex();
        }
        return $this;
    }

    static function createFibo(int $indice = 0, $isMmaxNumber = true)
    {
        return new Fibo($indice, $isMmaxNumber);
    }


    /**
     * Process the list of element refering to $maxNumber
     */
    private function processList()
    {
        while (
            (end($this->tab) < $this->maxNumber)
            || (
                empty($this->tab)
                && $this->maxNumber > 0
            )
        ) {
            $this->goToNext();
        }
        if (end($this->tab) > $this->maxNumber) {
            $this->goToPrev();
        }
        $this->index = count($this->tab);
    }

    /**
     * Process the list of element refering to $index
     */
    private function processListByIndex()
    {
        for ($i = 0; $i < $this->index; $i++) {
            $this->goToNext();
        }
        $this->maxNumber = end($this->tab);
    }

    /**
     * Process one step forward on the list
     */
    public function goToNext()
    {
        if (count($this->tab) < 2) {
            array_push($this->tab, 1);
        } else {
            array_push($this->tab, end($this->tab) + prev($this->tab));
        }
        return $this;
    }

    /**
     * Process one step back on the list
     */
    public function goToPrev()
    {
        array_pop($this->tab);
        return $this;
    }


    /**
     * Return the list in it actual state
     *
     * @return array
     */
    public function getTab(): array
    {
        return $this->tab;
    }

//    private function getByIndex(int $n): int
//    {
//        if ($n <= 1) {
//            return $n;
//        }
//        return $this->getByIndex($n - 1) + $this->getByIndex($n - 2);
//    }

}

